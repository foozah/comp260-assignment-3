﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour {

    public int nObjects = 5;
    public GameObject objectPrefab;
    public Rect spawnRect;
    private float objectPeriod = 5.0f;
    private float maxObjectPeriod = 5.0f;

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    public void CreateObject()
    {
        GameObject item = Instantiate(objectPrefab);
        item.transform.parent = transform;

        float x = spawnRect.xMin +
        Random.value * spawnRect.width;
        float y = spawnRect.yMin +
        Random.value * spawnRect.height;
        if (x >= -2 && x <= 2 && y >= -2 && y <= 2)
        {
            x -= 6;
            y += 6;
        }

        item.transform.position = new Vector2(x, y);
    }

    // Use this for initialization
    void Start () {
        int i = 0;
        while (i < nObjects)
        {
            GameObject item = Instantiate(objectPrefab);
            item.transform.parent = transform;
            item.gameObject.name = "Object " + i;

            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            if (x >= -2 && x <= 2 && y >= -2 && y <= 2)
            {
                x -= 6;
                y += 6;
            }
            item.transform.position = new Vector2(x, y);
            i++;
        }
	}
	
	// Update is called once per frame
	void Update () {
        objectPeriod = objectPeriod - Time.deltaTime;
        if (objectPeriod <= 0)
        {
            CreateObject();
            objectPeriod = maxObjectPeriod;
        }
	}
}
