﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

    void Start()
    {
        // find a player object to be the target by type
        PlayerMove player = (PlayerMove)
       FindObjectOfType(typeof(PlayerMove));
        target = player.transform;
    }

    public float speed = 1f;
    public Transform target;
    void Update()
    {
        // get the vector from the bee to the target
        // and normalise it.
        Vector2 direction = target.position - transform.position;
        direction = direction.normalized;
        Vector2 velocity = direction * speed;
        transform.Translate(velocity * Time.deltaTime);
    }

}
