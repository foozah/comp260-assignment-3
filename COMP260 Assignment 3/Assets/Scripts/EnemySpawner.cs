﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public int nEnemies = 1;
    public EnemyMove enemyPrefab;
    public Rect spawnRect;
    private float enemyPeriod = 3.0f;
    private float minEnemyPeriod = 3.0f;
    private float maxEnemyPeriod = 3.0f;
    public int difficulty = 1;

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    public void CreateEnemy()
    {
        // instantiate an enemy
        EnemyMove enemy = Instantiate(enemyPrefab);
        // attach to this object in the hierarchy
        enemy.transform.parent = transform;

        // move the enemy to a random position within
        // the spawn rectangle
        float x = spawnRect.xMin +
        Random.value * spawnRect.width;
        float y = spawnRect.yMin +
        Random.value * spawnRect.height;
        enemy.transform.position = new Vector2(x, y);
    }

    void Start()
    {
        // create enemies
        int i = 0;
        while (i < nEnemies)
        {

            // instantiate an enemy
            EnemyMove enemy = Instantiate(enemyPrefab);
            // attach to this object in the hierarchy
            enemy.transform.parent = transform;
            // give the enemy a name and number
            enemy.gameObject.name = "Enemy " + i;

            // move the enemy to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            enemy.transform.position = new Vector2(x, y);
            i++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        enemyPeriod = enemyPeriod - Time.deltaTime;
        if (enemyPeriod <= 0)
        {
            CreateEnemy();
            enemyPeriod = maxEnemyPeriod - (0.2f * difficulty);
        }

    }
}
